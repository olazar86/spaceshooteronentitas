using UnityEngine;
using Entitas;

public class GameController : MonoBehaviour
{
    
    public GameSetup gameSetup;
    
    private Systems _systems;
    void Start()
    {
        var context = Contexts.sharedInstance;

        context.game.SetGameSetup(gameSetup);
        
        _systems = CreateSystems(context);
        _systems.Initialize();
    }
    void Update()
    {
        _systems.Execute();
    }

    private Systems CreateSystems(Contexts contexts)
    {
        return new Feature("Game")
            .Add(new InitializeAsteroidSystem(contexts))
            .Add(new InitializePlayerSystem(contexts))
            
            .Add(new InputSystem(contexts))
            .Add(new ShootSystem(contexts))
            .Add(new HitAsteroidSystem(contexts))
            .Add(new MapAsteroidLevelToResourcesSystem(contexts))
            
            .Add(new InstantiateViewSystem(contexts))
            
            .Add(new RotateLaserSystem(contexts))
            .Add(new RotatePlayerSystem(contexts))
            .Add(new PlayerReplaceAccelerationSystem(contexts))
            .Add(new MoveSystem(contexts))
            .Add(new LaserCheckBorderSystem(contexts))
            .Add(new CheckBorderSystem(contexts))
            .Add(new ApplicationQuitSystem(contexts))
            .Add(new DestroySystem(contexts))
            ;
            
    }
}
