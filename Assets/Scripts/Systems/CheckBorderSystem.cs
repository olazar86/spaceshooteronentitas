using Entitas;
using System.Collections.Generic;
using UnityEngine;

public class CheckBorderSystem : ReactiveSystem<GameEntity>
{
    private Contexts _contexts;

    public CheckBorderSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }
    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.OutOfBorder));
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isOutOfBorder && (entity.isPlayer || entity.hasAsteroid);
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            var coord = _contexts.game.gameSetup.value.coordinates;
            var position = entity.position.value;
            if (position.x <= coord[0]) 
            {
                position.x = coord[1];
            }
            if (position.x >= coord[1])
            {
                position.x = coord[0];
            }
            if (position.y <= coord[2])  
            {
                position.y = coord[3];
            }
            if (position.y >= coord[3])
            {
                position.y = coord[2];
            }
            entity.ReplacePosition(position);
            entity.isOutOfBorder = false;
        }
    }
}
