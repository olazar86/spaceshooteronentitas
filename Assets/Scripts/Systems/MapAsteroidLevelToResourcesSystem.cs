using Entitas;
using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

public class MapAsteroidLevelToResourcesSystem : ReactiveSystem<GameEntity>
{
    private Contexts _contexts;

    public MapAsteroidLevelToResourcesSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }

    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.Asteroid);
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.hasAsteroid;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        var setup = _contexts.game.gameSetup.value;
        foreach (var entity in entities)
        {
            entity.AddResource(
                MapAsteroidLevelToResources(entity.asteroid.level, setup));
            var speed = _contexts.game.gameSetup.value.asteroidSpeed;
            var randomAngle = Random.Range(0f, 2f);
            entity.AddAcceleration(new Vector3(
                speed * Mathf.Cos(randomAngle),
                speed * Mathf.Sin(randomAngle),
                0f));
        }
    }

    private GameObject MapAsteroidLevelToResources(int level, GameSetup setup)
    {
        switch (level)
        {
            case 1:
                return setup.micro[Random.Range(0, setup.micro.Length)];
            case 2:
                return setup.midi[Random.Range(0, setup.midi.Length)];
            default:
                return setup.bigs[Random.Range(0, setup.bigs.Length)];
            
        }
    }
}
