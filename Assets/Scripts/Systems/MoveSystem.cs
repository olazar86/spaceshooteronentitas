using System.Numerics;
using Entitas;
using Unity.VisualScripting;
using UnityEngine;
using Vector3 = UnityEngine.Vector3;

public class MoveSystem : IExecuteSystem
{
    private Contexts _contexts;
    private IGroup<GameEntity> _group;

    public MoveSystem(Contexts contexts)
    {
        _contexts = contexts;
        _group = contexts.game.GetGroup((GameMatcher.AllOf(
            GameMatcher.Acceleration, GameMatcher.Position)));
    }

    public void Execute()
    {
        foreach (var entity in _group)
        {
            var entityPosition = entity.position.value;
            var acceleration = entity.acceleration.value;
            var position = entityPosition;
            var coord = _contexts.game.gameSetup.value.coordinates;
            if (position.x > coord[1] || position.x < coord[0] || position.y > coord[3] || position.y < coord[2])
            {
                entity.isOutOfBorder = true;
            }
            position += acceleration * Time.deltaTime;
            entity.ReplacePosition(position);
            entity.view.value.transform.position = position;
        }
    }
}
