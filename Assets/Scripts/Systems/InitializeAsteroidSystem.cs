using Entitas;
using UnityEngine;

public class InitializeAsteroidSystem : IInitializeSystem
{
  private Contexts _contexts;

  public InitializeAsteroidSystem(Contexts contexts)
  {
    _contexts = contexts;
  }

  public void Initialize()
  {
    for (int i = 0; i < 4; i++)
    {
      var entity = _contexts.game.CreateEntity();
      
      entity.AddAsteroid(3);
      entity.AddInitialPosition(new Vector3(0f, 0f, 0f));
      entity.AddPosition(new Vector3(Random.Range(-10f, 10f),
        Random.Range(-4f, 4f), 0f));
      
    }
  }
}
