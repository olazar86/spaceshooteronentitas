using Entitas;
using System.Collections.Generic;


public class LaserCheckBorderSystem : ReactiveSystem<GameEntity>
{
    private Contexts _contexts;

    public LaserCheckBorderSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }
    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.OutOfBorder, GameMatcher.Laser));
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isOutOfBorder && entity.isLaser;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        foreach (var entity in entities)
        {
            entity.isDestroy = true;
        }
    }
}
