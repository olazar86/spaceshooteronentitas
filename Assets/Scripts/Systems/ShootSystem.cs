using Entitas;
using UnityEngine;

public class ShootSystem : IExecuteSystem
{
    private Contexts _contexts;

    public ShootSystem(Contexts contexts)
    {
        _contexts = contexts;
    }
    
    public void Execute()
    {
        if (Input.GetButtonDown("Fire"))
        {
            var entity = _contexts.game.CreateEntity();
            var setup = _contexts.game.gameSetup.value;
            var playerTransform = _contexts.game.playerEntity.view.value.transform;
            var playerForward = playerTransform.up;
            entity.AddResource(setup.laser);
            entity.AddAcceleration(playerForward * setup.laserSpeed);
            entity.AddPosition(playerTransform.position);
            entity.isLaser = true;

        }
    }

}
