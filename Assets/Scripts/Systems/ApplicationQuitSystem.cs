using System.Collections.Generic;
using Entitas;
using UnityEngine;

public class ApplicationQuitSystem : ReactiveSystem<GameEntity>
{
    private Contexts _contexts;

    public ApplicationQuitSystem(Contexts contexts) : base(contexts.game)
    {
        _contexts = contexts;
    }
    protected override ICollector<GameEntity> GetTrigger(IContext<GameEntity> context)
    {
        return context.CreateCollector(GameMatcher.AllOf(GameMatcher.Player, GameMatcher.Destroy));
    }

    protected override bool Filter(GameEntity entity)
    {
        return entity.isPlayer && entity.isDestroy;
    }

    protected override void Execute(List<GameEntity> entities)
    {
        Application.Quit();
    }
}
