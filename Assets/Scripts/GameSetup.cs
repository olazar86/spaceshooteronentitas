using UnityEngine;
using Entitas.CodeGeneration.Attributes;

[CreateAssetMenu]
[Game, Unique]
public class GameSetup : ScriptableObject
{
    [Header("PlayerSetup")]
    public GameObject player;
    public float rotationSpeed = 180f;
    public float playerMovementSpeed = 5f;

    [Header("LaserSetup")]
    public GameObject laser;
    public float laserSpeed = 10f;
    
    [Header("PlayingFieldSetup")]
    public float[] coordinates = { -12, 12, -5, 5 };
    
    [Header("Asteroids")]
    public GameObject[] bigs;
    public GameObject[] midi;
    public GameObject[] micro;
    public float asteroidSpeed = 0.5f;
}
