using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmitTriggerEntityBehaviour : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D col)
    {
        var entity = Contexts.sharedInstance.game.CreateEntity();
        entity.AddCollision(gameObject, col.gameObject);
    }
}
